from flask import Flask, json, render_template, make_response, jsonify, request

app = Flask(__name__)

PORT = 3200
HOST = '0.0.0.0'

INFO = {
    "students": [
        {
            "id": 1,
            "university": "UTPL",
            "course": "Procesos de Ingeniería de Software",
            "name": "Jorge Alcivar Hurtado Duarte",
            "period": "Abr/Ago 2021",
        },
        {
            "id": 2,
            "university": "UTPL",
            "course": "Procesos de Ingeniería de Software",
            "name": "Ariana Fernanda Medina Hualpa",
            "period": "Abr/Ago 2021",
        },
    ]
}

# GET METHOD


@app.route("/")
def home():
    return "<h1>Recommendation Service</h1>"


@app.route("/temp")
def template():
    return render_template('index.html')


@app.route("/qstr")
def query_string():
    if request.args:
        req = request.args
        res = {}
        for key, value in req.items():
            res[key] = value
        res = make_response(jsonify(res), 200)
        return res

    res = make_response(jsonify({"error": "No query string"}), 400)
    return res


@app.route("/json")
def get_json():
    res = make_response(jsonify(INFO), 200)
    return res


@app.route("/json/<collection>")
def get_data(collection):
    if collection in INFO:
        res = make_response(jsonify(INFO), 200)
        return res

    res = make_response(jsonify({"error": "Not found"}), 404)
    return res

# POST METHOD


@app.route("/json/students/", methods=["POST"])
def create_col():
    req = request.get_json()
    INFO["students"].append(req)
    res = make_response(jsonify({"message": "Student created"}), 201)
    return res

# PUT METHOD


@app.route("/json/<collection>/<member>", methods=["PUT"])
def put_col_mem(collection, member):

    req = request.get_json()

    if collection in INFO:
        if member:
            print(req)
            INFO[collection][member] = req["new"]
            res = make_response(jsonify({"res": INFO[collection]}), 200)
            return res

        res = make_response(jsonify({"error": "Not found"}), 404)
        return res

    res = make_response(jsonify({"error": "Not found"}), 404)
    return res

# DELETE METHOD


@app.route("/json/<collection>", methods=["DELETE"])
def delete_col(collection):

    if collection in INFO:
        del INFO[collection]
        res = make_response(jsonify(INFO), 200)
        return res

    res = make_response(jsonify({"error": "Collection not found"}), 404)
    return res


if __name__ == "__main__":
    print("Server running in port %s" % (PORT))
    app.run(host=HOST, port=PORT)
